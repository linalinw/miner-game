#ifndef SOLDIER_H
#define SOLDIER_H

#include "Game_Object.h"
#include "Cart_Point.h"
#include "Cart_Vector.h"
#include "Person.h"

class Soldier: public Person{
private:
    char display_code;
    int id_num;
    int attack_strength;
    
    //the number of attack points delivered to the target with each "hit." Initial value is 2.
    
    double range;
    //the target must be within this distance to start attacking, and stay within this distance to continue attacking. Initial value is 2.0.
    int health;
    Person * target;
    //the object being attacked.
    
public:
    void start_attack(Person * in_target);
    //if the distance to the target is less than or equal to the range, output an appropriate message (“Clang!”), save the target pointer, and set the state to 'a' for attack. If it is too far away, output a message: "Target is out of range" and do nothing.
    
    bool update();
    //This function updates the Soldier object as follows:
    /*
     - state ‘x’: do nothing and return false.
     - state ‘s’: do nothing and return false;
     - state ‘m’: same thing as in the Miner::update() function for this state.
     - state ‘a’: Check the distance to the target. If it is out of range, print a message, set the state to ‘s’ and return true. If it is in range, then check whether the target is still alive. If not, output an appropriate message like "I triumph!", set the state to ‘s’ and return true. If the target is still alive, output a message like “Clang!" and call the target's take_hit function with the attack_strength as an argument, stay in the state, and return false.
     */
    
    void show_status();
    /*
     It outputs something like "Soldier status:", then calls Person::show_status(), then outputs whether the object is attacking.
     */
    
    void take_hit(int attack_strength, Person *attacker_ptr);
    
    //maybe needed
    Soldier();
    Soldier(int in_id, Cart_Point in_loc);
    
    
    ~Soldier();
    
};

#endif
