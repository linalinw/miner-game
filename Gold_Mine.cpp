#include <iostream>
#include "Gold_Mine.h"

using namespace std;

Gold_Mine::Gold_Mine() : Game_Object('G', 0){
    //display_code ='G' and set initial value of display code
    //id_num = 0; //set initial value of id_num
    state = 'f'; // initial state is "full of gold"
    location.x = 0.0; // initialized the location (0,0)
    location.y = 0.0;
    amount = 100.0;
    
    cout << "Gold_Mine default constructed." << endl;
}

//create a non default mine
Gold_Mine::Gold_Mine(int inputId, Cart_Point inputLoc) : Game_Object('G', inputId, inputLoc){
    id_num = inputId;
    location.x = inputLoc.x;
    location.y = inputLoc.y;
    amount = 100.0;
    state = 'f';
    cout << "Gold_Mine constructed." << endl;
}


//check if the mine is empty.
bool Gold_Mine::is_empty(){
    if (amount == 0){
        return true;
    }
    else{
        return false;
    }
}

//create a function to calculate the amount of gold left in the mine
double Gold_Mine::dig_gold(double amount_to_dig){
    if (amount >= amount_to_dig){
        amount = amount - amount_to_dig;
        return amount_to_dig;
    }
    
    else{
        double temp; //set a temporary variable to get amount before it'sset to zero
        temp = amount;
        amount = 0; // set the amount of gold in the mine to zero.
        return temp;
    }
}

//update the status of the Gold_Mine when it's empty.
bool Gold_Mine::update(){
    if(amount == 0 && state == 'f'){
        state = 'e';
        display_code = 'g';
        cout << "Gold Mine" << id_num << " is depleted." << endl;
        return true;
    }
    
    else{
        return false;
    }
}

void Gold_Mine::show_status(){
    //Gold Mine status: G1 at location (1,20) containing 100.
    cout << "Gold Mine status: " << display_code <<id_num << " at location " << location << " Contains " << amount <<"."<< endl;
}

Gold_Mine::~Gold_Mine(){
    cout << "Gold_Mine destructed."<< endl;
}

//PA4 addition
double Gold_Mine::get_amount(){
    return amount;
}//get the amount in the gold mine



