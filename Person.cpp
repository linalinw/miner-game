#include <iostream>

using namespace std;

#include "Person.h"

Person::Person(): Game_Object('p' , 0){

}

Person::Person(char in_code): Game_Object(in_code, 0){
    speed = 5;
    health = 5; //PA4 added
    if(display_code == 'I'||display_code =='i'){
        health = 6;
    }
    cout << "Person default constructed." << endl;
}

Person::Person(char in_code,int in_id,Cart_Point in_loc): Game_Object(in_code, in_id, in_loc){
    speed = 5;
    health = 5; //PA4 added
    cout << "Person constructed." << endl;
}

void Person::start_moving(Cart_Point dest){
    if (this -> is_alive()) { //check if the Person is still alive
        setup_destination(dest);
        if (dest.x != location.x || dest.y != location.y){
            state = 'm'; //set the state to Moving.
            cout << "Moving " << get_id() << " to " << dest << endl;
            cout << display_code << get_id() << ": On my way." << endl;
        }
        
        else{//destination == current location
            cout << "Moving " << get_id() << " to " << dest << endl;
            cout << display_code << get_id() << ": I'm already there. see?" << endl;
        }
    }
    
    else {
        cout << "Person you are trying to order is no longer of this world" << endl;
    }
    
    //PA4 addition: added an if-else statement to check if the person is dead or not.
    
}

void Person::stop(){
    state = 's'; //set the state set to Stopped
    //setup_destination(get_location());
    delta.x = 0;//added part
    delta.y = 0;//added part
    cout << display_code << get_id() << ": All right." << endl;
}

void Person::show_status(){
    //Miner status: M1 at (6.27247,5.83537) moving at speed of 5 towards (10,20) at each step of <1.27247,4.83537>
    if (state == 'm'){
        
        cout << display_code << get_id() << " at " << get_location() << " moving at speed of " << speed << " towards " << destination << " at each step of " << delta << " with health of " << health;
    }
    
    //Miner status: M2 at (10,1) moving at speed of 5 towards (0,0) at each step of <0,0> is stopped.
    else {
        cout << display_code << get_id() << " at " << get_location();
    }
}

bool Person::update_location(){
    //check if the object is within one step of its destinantion
    if (fabs(destination.x - location.x) <= fabs(delta.x) && fabs(destination.y - location.y) <= fabs(delta.y)){
        location = destination;
        cout << display_code << get_id() << ": I'm there!" << endl;
        //M1: I’m there!
        return true;
    }
    
    //
    else{
        location = location + delta;
        cout << display_code << get_id() <<": step..." << endl;

        return false;
    }
}

void Person::setup_destination(Cart_Point dest){
    destination = dest;
    delta =  (destination - location) * (speed / cart_distance(destination, location));
   
}

Person::~Person(){
    cout << "Person destructed." << endl;
}

void Person::start_mining(){
    cout << "Sorry, I can't work a mine." << endl;
}

//PA4 addition
void Person::take_hit(int attack_strength, Person *attacker_ptr){
    Cart_Vector escape;
    Cart_Point movingto;
    
    //check for escape routes
    escape = (this -> get_location()) - (attacker_ptr -> get_location());
    movingto = this -> get_location() + (escape * 1.5);
    
    
     //check sample output.
    health = health - attack_strength; //needs to be double-checked.
    
    if (health < 5){//change the display code from uppercase to lowercase
        display_code =(char)((int)display_code+32);
    }
    
    if(health <= 0){
        cout << get_id()<<"Arrggh!" << endl;
        state = 'x';
    }
    
    else{
        //no changes to state
        cout << display_code<< this-> get_id() << ": Ouch!"<< endl;
        cout << display_code << this-> get_id() << ": I don't wanna fight!" << endl;
        start_moving(movingto);
    }
}

void Person::start_attack(){
    cout << "I can‘t attack." << endl;
}


void Person::start_inspecting(){//inspector stuff
    cout << "I can‘t inspect." << endl;
}

int Person::get_health(){
    return health;
}

char Person::get_state(){
    return state;
}



