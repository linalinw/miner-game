#include <iostream>
#include "View.h"

using namespace std;

View::View(){
    size = 11;
    scale = 2.0;
    origin.x = 0.0;
    origin.y = 0.0;
    
}

bool View::get_subscripts(int &ix, int &iy, Cart_Point location){
    ix = (location.x - origin.x)/scale;
    iy = (location.y - origin.y)/scale;
    
    if (ix < size && iy < size){
        return true;
    }
    
    else{
        cout << "An object is outside the display." << endl;
        return false;
    }
}


void View::clear(){
    for (int i = 0; i < size+1; i++){
        for (int j = 0; j < size+1; j++){
            grid[i][j][0] = '.';
            grid[i][j][1] = ' ';
        }
    }
}


void View::plot(Game_Object *ptr){
    char plotting[2];
    int ix, iy;
    
    //check if the pointer is on the grid.
    if(get_subscripts(ix, iy, ptr -> get_location())){
        
        if (grid[iy][ix+1][0] != '.' && grid[iy][ix+1][1] != ' '){ //check if the array has already a pointer at that location.
            grid[iy][ix+1][0] = '*';
            grid[iy][ix+1][1] = ' ';
            return;
        }
        else if(grid[iy][ix+1][0] == '*' && grid[iy][ix+1][1] == ' '){
            grid[iy][ix+1][0] = '*';
            grid[iy][ix+1][1] = ' ';
            return;
        }
        
        ptr -> drawself(plotting); //plot the subscript on the graph
        grid[iy][ix+1][0] = plotting[0];//shift my plot to the right
        grid[iy][ix+1][1] = plotting[1];//shift my plot to the right
    }
}

void View::draw(){
    //print the vertical scale.
    for (int i = (view_maxsize)/2; i > 0; i--){
        for (int j = 0; j < (size + 1); j++){
            if(i%((int)scale)==0 && j == 0){
                cout << setw(2)<< left << (i*2);
            }
            
            else if(i%((int)scale*2) != 0 && j == 0){
                cout << ' ' << ' ';
            }
            
            else{
                cout << grid[i][j][0];
                cout << grid[i][j][1];
            }
        }
        cout << endl;
    }
    
    //print the last line of the the y-axis
    for (int j = 0; j < (size + 1); j++){
        if(j==0){
            cout << setw(2)<< left << '0';
        }
        
        else{
            cout << grid[0][j][0];
            cout << grid[0][j][1];
        }
    }
    cout << endl;
    
    //print the horizontal scale
    for(int i = 0; i < (view_maxsize+1); i++){
        for (int j = 0; j < (view_maxsize+1);j++){
            if(i == view_maxsize){
                //adjust the origin
                if(j == 0){
                    cout << setw(3)<< right << j<<"   ";
                }
                
                //printing the scale normally
                else if (j% ((int)scale*2) == 0){
                    cout << setw(4) << left << j;
                }
            }
        }
    }
    cout << endl;
}
