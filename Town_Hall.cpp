#include <iostream>

using namespace std;

#include "Town_Hall.h"

//define the default Town_Hall constructor
Town_Hall::Town_Hall() : Game_Object('t', 1){
    //set initial value of display code and set initial value of id_num
    state = 'o';
    id_num = 1;
    location.x = 0.0; // initialized the location (0,0)
    location.y = 0.0;
    
     amount = 0.0; //initialize the value to 0.0
    cout << "Town_Hall default constructed." << endl;
}

//define the non-default Town_Hall constructor
Town_Hall::Town_Hall(int inputId, Cart_Point inputLoc) : Game_Object('t', inputId, inputLoc){
    state = 'o'; 
    id_num = inputId; //set the id_num of the town hall to inputId.
    amount = 0.0; //initialize the value to 0.0
    location.x = inputLoc.x;
    location.y = inputLoc.y;
    
    cout << "Town_Hall constructed." << endl;
}

//define the function to add the gold to the town hall
void Town_Hall::deposit_gold(double deposit_amount){
    amount += deposit_amount;
}

//define the function to upgrade the town hall
bool Town_Hall::update(){
    if (amount >= 50 && display_code == 't'){
        state = 'u'; //u is for upgarded
        display_code = 'T';
        cout <<display_code<< id_num << " has been upgraded." << endl;
        return true;
    }
    
    else{
        return false;
    }
}

//define the function to show the status of the town hall
void Town_Hall::show_status(){
    cout <<"Town Hall status: " << display_code << id_num << " at " << location<<" contains "<< amount << " of gold.";

    //display message about whether the town hall s upgraded or not
    if (display_code == 'T'){
        cout << "Upgraded." << endl;
    }
    if (display_code == 't'){
        cout << " Not yet upgraded." << endl;
    }
}

Town_Hall::~Town_Hall(){
    cout << "Town_Hall destructed." << endl;
}