#include <iostream>
#include <cmath>
#include "Cart_Point.h"
#include "Cart_Vector.h"

using namespace std;

Cart_Point::Cart_Point()
{
	x = 0.0;
	y = 0.0;
}

Cart_Point::Cart_Point(double inputx, double inputy)
{
	x = inputx ; // assign x the variable inputx
	y = inputy ; // assign y the variable inputy
	
};



double cart_distance(Cart_Point p1, Cart_Point p2){
	double distance;

	// calculate the distance between the points Cart_Point p1 and Cart_Point p2
	distance = sqrt(pow ((p1.y - p2.y),2) + pow ((p1.x - p2.x),2));
	
	return distance;

}

//Stream output operator (<<): produces output formatted as (x, y) 
std::ostream& operator <<( std::ostream& os, const Cart_Point& p1){
	os << "(" << p1.x << ", " << p1.y << ")";
	return os;
}

//Addition operator (+): p1 + v1 returns a Cart_Point object with x = p1.x + v1.x and y = p1.y + v1.y 
Cart_Point operator + ( const Cart_Point& p1, const Cart_Vector& v1){
	Cart_Point cart_point;
	cart_point.x = p1.x + v1.x;
	cart_point.y =  p1.y + v1.y;
	return cart_point;
}

//Subtraction operator (-): p1 - p2 returns a Cart_Vector object with x = p1.x - p2.x and y = p1.y - p2.y 
Cart_Vector operator-( const Cart_Point& p1, const Cart_Point& p2){
	Cart_Vector cart_vector;
	cart_vector.x = p1.x - p2.x;
	cart_vector.y = p1.y - p2.y;
	return cart_vector;
}