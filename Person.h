#ifndef Person_h
#define Person_h

#include "Game_Object.h"
#include "Cart_Point.h"
#include "Cart_Vector.h"
#include <math.h>


class Person: public Game_Object{
public:
    Person();
    Person(char in_code);
    Person(char in_code, int in_id, Cart_Point in_loc);
    void start_moving(Cart_Point dest);
    void stop();
    void show_status();
    virtual void start_mining();
    virtual ~Person();
    
    //PA4 starts from here.
   //It returns true if the state is not ‘x’, false if it is ‘x’
    
    virtual void take_hit(int attack_strength,  Person *attacker_ptr);
    /*
     It subtracts the attack_strength from the health. If the resulting new value of health is less than or equal to zero, print a dramatic final message (“Arrggh!”), and set the state to ‘x’. Otherwise, leave the state unchanged and output an appropriate message:
     "Ouch!
     
     modify show_status to show health if alive, "is dead" if not.
     */
    
    int get_health();
    char get_state();
    
    virtual void start_attack();
    //It does nothing but output a message: "I can‘t attack."
    
    virtual void start_inspecting();
    //It does nothing but output a message: "I can‘t inspect."


protected:
    bool update_location();
    void setup_destination(Cart_Point dest);
    
private:
    double speed;
    Cart_Point destination;
    Cart_Vector delta;
    
    //PA4 starts from here.
    int health;
};


#endif
