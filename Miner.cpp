#include <iostream>

#include "Miner.h"

using namespace std;

Miner::Miner(): Person('M'){
    display_code = 'M';
    amount = 0.0; //initialize amount to zero;
    mine = NULL;
    home = NULL;
    cout << "Miner default constructed. " << endl;
}

Miner::Miner(int in_id, Cart_Point in_loc): Person('M', in_id, in_loc){
    display_code = 'M';
    amount = 0.0;
    mine = 0;
    home = 0;
    cout << "Miner constructed." << endl;
}

bool Miner::update(){
    
    switch(state){
        
        case 's':
            state = 's';
            return false;
          
            
        case 'm':
            if(update_location()){
                state = 's';//did
                return true;
            }
            else{
                state = 'm';
                return false;
            }
            break;
            
        case 'o':
            if(update_location()){
                state = 'g';
                return true;
            }
            
            else
                return false;
            break;
            
        case 'g':
            amount = mine -> dig_gold();
            cout << display_code << get_id() << ": Got " << amount << " gold." << endl;
            setup_destination(home -> get_location());
            state = 'i';
            return true;
            break;
            
        case 'i':
            if(update_location()){
                state = 'd';
                return true;
            }
            
            else
                return false;
            break;
            
        case 'd':
            state = 'm';
            cout << display_code << get_id() << ": Deposit " << amount << " of gold." << endl;
            home -> deposit_gold(amount);
            
            amount = 0.0;
            if (mine -> is_empty()){
                state = 's';
                cout << "More work?" << endl;
                return true;
            }
            
            else{
                state = 'o';//did
                setup_destination(mine -> get_location());
                cout << display_code << get_id() << ": Going back for more." << endl;
                return true;
            }
            break;
            
            //PA4 stuff
            /*If the state is ‘x’, stay in that state, and do nothing except return false*/
        
        case 'x': //check naming of the case here.
            state = 'x';
            return false;
    }
    return false;
}

void Miner::show_status(){
    cout << "Miner status: ";
    Person::show_status();
    switch(state){
        case 's': //Miner does nothing
            cout << " is stopped." << endl;
            break;
            
        case 'm': //moving to a destination
            cout << endl;
            break;
        
        case 'o': //for outbound mining
            cout << " is outbound to a mine." << endl;
            break;
            
        case 'g': // for getting gold
            cout << " is getting gold from mine." << endl;
            break;
            
        case 'i': // for inbound mining.
            cout << " is inbound to home with load: " << amount << endl;
            break;
            
        case 'd': // depositing gold
            cout << " is depositing gold." << endl;
            break;
            
        case 'x': // dead.
            cout << "is dead." << endl;
            break;
            
    }
}


void Miner::start_mining(Gold_Mine *inputMine, Town_Hall *inputHome){
    mine = inputMine;
    home = inputHome;
    if (this->is_alive()) {//check if miner is alive
        setup_destination(inputMine->get_location());
        state = 'o'; // "Outbound"
    }
    
    else {
        cout << "Person you are trying to order is no longer of this world" << endl;
        //check what sample output says about that
            }
    
    //PA4 addition: added the if-else statement to check if miner is dead or not. if dead do nothing. else change stuff
}

Cart_Point Miner::get_location(){
    //cout << "Miner status: ";
    show_status();
    cout << endl;
    return location;
}

Miner::~Miner(){
    cout << "Miner destructed." << endl;
}
