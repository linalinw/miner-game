#include "Model.h"
#include <iostream>

using namespace std;

Model::Model(){
    time = 0;
    
    //define each cart point for the different game_objects
    Cart_Point p1(5,1);//Miner1 location
    Cart_Point p2(10,1);//Miner2 location
    Cart_Point p3(1,20);//Gold_Mine1 location
    Cart_Point p4(10,20);//Gold_Mine2 location
    Cart_Point p5(0,0);//Town_Hall location
    Cart_Point p6(5,15);//Soldier 1 location (PA4 added)
    Cart_Point p7(10,15);//Soldier 2 location (PA4 added)
    Cart_Point p8(5,5);//Soldier 2 location (PA4 added)
    
    
   
    //add the pointers in the pointer list.
    Miner* m1 = new Miner(1,p1);
    object_ptrs.push_back(m1);
    person_ptrs.push_back(m1);
    active_ptrs.push_back(m1);

    
    Miner* m2 = new Miner(2,p2);
    object_ptrs.push_back(m2);
    person_ptrs.push_back(m2);
    active_ptrs.push_back(m2);
    
    Soldier* s1 = new Soldier(3,p6);
    object_ptrs.push_back(s1);
    person_ptrs.push_back(s1);
    active_ptrs.push_back(s1);
    
    
    Soldier* s2 = new Soldier(4,p7);
    object_ptrs.push_back(s2);
    person_ptrs.push_back(s2);
    active_ptrs.push_back(s2);
    
    Inspector* i1 = new Inspector(5,p8);
    object_ptrs.push_back(i1);
    person_ptrs.push_back(i1);
    active_ptrs.push_back(i1);
    
    Gold_Mine* g1 = new Gold_Mine(1,p3);
    object_ptrs.push_back(g1);
    mine_ptrs.push_back(g1);
    active_ptrs.push_back(g1);

    
    Gold_Mine* g2 = new Gold_Mine(2,p4);
    object_ptrs.push_back(g2);
    mine_ptrs.push_back(g2);
    active_ptrs.push_back(g2);

    
    Town_Hall* t1 = new Town_Hall();
    object_ptrs.push_back(t1);
    hall_ptrs.push_back(t1);
    active_ptrs.push_back(t1);
    
    
    cout << "Model default constructed." << endl;
    
}

//Model destructor
Model::~Model(){
    //delete the game object pointers
    list<Game_Object*>::iterator i;
    for (i = object_ptrs.begin(); i != object_ptrs.end(); ++i){
        delete *i;
    }
    
    cout << "Model destructed." << endl;
}

//call the update function of every other classes
bool Model::update(){
    time++;
    int updating = 0;
    list<Game_Object*>::iterator i;
    for (i = active_ptrs.begin(); i != active_ptrs.end(); ++i){
        if ((*i) -> is_alive()){
            if((*i)->update()){
                updating++;
                cout <<"Time: "<<time << endl;
            }
        }//update the objects that are alive
        
        else{
            active_ptrs.remove(*i);
            //cout << "Dead object removed" << endl;
            break;
        }
    }
    
    if(updating>4)
        return true;
    
    else
        return false;
}

//calls the show_status() function of the every other classes
void Model::show_status(){
    list<Game_Object * >::iterator i;
    for (i = object_ptrs.begin(); i != object_ptrs.end(); ++i){
        (*i) -> show_status();
    }
    /*
    list<Person*>::iterator i;
    for (i = person_ptrs.begin(); i != person_ptrs.end(); ++i){
        (*i) -> show_status();
    }
    
    list<Gold_Mine*>::iterator j;
    for (j = mine_ptrs.begin(); j != mine_ptrs.end(); ++j){
        (*j) -> show_status();
    }

    list<Town_Hall*>::iterator k;
    for (k = hall_ptrs.begin(); k != hall_ptrs.end(); ++k){
        (*k) -> show_status();
    }
     */

}

//get the person pointers
Person* Model::get_Person_ptr(int id){
    list<Person*>::iterator i;
    for (i = person_ptrs.begin(); i != person_ptrs.end(); ++i){
        if((*i)->get_id() == id)
            return (*i);
    }
    return NULL;
}

//get the gold mines pointers
Gold_Mine* Model::get_Gold_Mine_ptr(int id){
    list<Gold_Mine*>::iterator j;
    for (j = mine_ptrs.begin(); j != mine_ptrs.end(); ++j){
        if((*j)->get_id() == id)
            return (*j);
    }
    return 0;
}

//get the town hall pointers
Town_Hall* Model::get_Town_Hall_ptr(int id){
    list<Town_Hall*>::iterator k;
    for (k = hall_ptrs.begin(); k != hall_ptrs.end(); ++k){
        if((*k)->get_id() == id)
            return (*k);
    }
    return 0;
}

void Model::display(View &view){
    view.clear();
    list<Game_Object*>::iterator j;
    for (j = active_ptrs.begin(); j != active_ptrs.end(); ++j){
        view.plot(*j);
    }
    
    view.draw();
}

//adds a new inspector, new soldier or miner to the list
void Model::add_person(Person *p){
    object_ptrs.push_back(p);
    person_ptrs.push_back(p);
    active_ptrs.push_back(p);
}

//adds a new gold mine to the list
void Model::add_mine(Gold_Mine * m){
    object_ptrs.push_back(m);
    mine_ptrs.push_back(m);
    active_ptrs.push_back(m);

}

//adds a new town hall to the list
void Model::add_townhall(Town_Hall *t){
    object_ptrs.push_back(t);
    hall_ptrs.push_back(t);
    active_ptrs.push_back(t);
}

//returns a list of the mine used for inspectors
list<Gold_Mine*> Model::getmine(){
    return mine_ptrs;
}