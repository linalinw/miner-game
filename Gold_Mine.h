#ifndef GOLDMINE_h
#define GOLDMINE_h

#include "Game_Object.h"
#include "Cart_Point.h"

class Gold_Mine: public Game_Object{
    
private:
    int id_num;
    double amount;
    
public:
    //constructors
    Gold_Mine();
    Gold_Mine(int inputId, Cart_Point inputLoc);

    //public members variable
    bool is_empty();
    double dig_gold(double amount_to_dig = 35.0);
    bool update();
    void show_status();
    virtual ~Gold_Mine();
    
    //PA4 addition for inspector
    double get_amount();
};

#endif /* Gold_Mine_h */
