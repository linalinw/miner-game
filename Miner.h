#ifndef Miner_h
#define Miner_h

#include "Game_Object.h"
#include "Person.h"
#include "Gold_Mine.h"
#include "Town_Hall.h"
#include "Cart_Vector.h"
#include "Cart_Point.h"

class Miner: public Person{
public:
    Miner();
    /*
     It invokes Person(‘M’), and initializes amount to 0, and the mine and
     home pointers to NULL. As before, it outputs a message: “Miner default
     constructed.”
     */
    
    Miner(int in_id, Cart_Point in_loc);
    /*
     It invokes Person(‘M’, in_id, in_loc), and initializes amount to 0, and the mine and
     home pointers to 0. As before, it outputs a message: “Miner constructed.”
     */
    
    bool update();
    /*
     Returns true when the state is changed.
     Updates the status of the object on each time unit (See “How the Miner Behaves”
     for details).
     */
    
    void start_mining(Gold_Mine* inputMine, Town_Hall* inputHome);
    /*
     - Tells the Miner to start mining: move gold from the mine to home, which are specified by pointers to a Gold_Mine and Town_Hall objects.
     - Calls setup_destination() function.
     - Sets the state to ‘o’ for “Outbound.”
     - Prints “Miner (id) mining at Gold_Mine (id) and depositing at Town_Hall (id).” and another message “(display_code)(id): Yes, my lord.”
    */
    
    Cart_Point get_location();
    /*
     - Returns a Cart_Point containing the location of the object.
     - It outputs something: “Miner status:”, then calls Person::show_status() and then
     displays the information about the Miner-specific states of inbound, outbound,
     getting gold, and depositing gold.
    */
    
    void show_status();
    
    ~Miner();
    
private:
    char display_code;
    int id_num;
    double amount;
    Cart_Point location;
    Gold_Mine *mine;
    Town_Hall *home;
    
};

#endif
