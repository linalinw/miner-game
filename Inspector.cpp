#include <iostream>
#include "Inspector.h"

using namespace std;

Inspector::Inspector() : Person('I'){
    display_code = 'I';
    cout << "Inspector default constructed." << endl;
}

Inspector::Inspector(int in_id, Cart_Point in_loc) : Person('I', in_id, in_loc){
    display_code = 'I';
    cout << "Inspector Constructed." <<endl;
}

void Inspector::show_status(){
    cout << "Inspector status: ";
    Person::show_status();
    switch (state) {
        case 's': // does nothing
            cout << " is stopped." << endl;
            break;
        
        case 'm': //moving to a destination
            cout << endl;
            break;
        
        case 'o':
            cout << " is doing outbound inspection." <<endl;
            break;
        
        case 'i':
            cout << " is doing inbound inspection." << endl;
            break;
        
        case 'x': // dead.
            cout << "is dead." << endl;
            break;
    }
}

bool Inspector::update(){
    switch (state) {
        case 's': //inspector is stopped;
            state = 's';
            return false;
        
        case 'm':
            if(update_location()){
                state = 's';//did
                return true;
            }
            else{
                state = 'm';
                return false;
            }
            break;
        
        case 'x': //check naming of the case here.
            state = 'x';
            return false;
        
        case 'o': //double check the way the command will work
            init_pos =this-> get_location();
            start_inspecting(mine);
            state = 'i';
            return true;
        
        case 'i'://double check the way the command will work.
            finish_inspecting(mine);
            if((this->get_location()).x == init_pos.x && (this->get_location()).y == init_pos.y)
                cout << "Inspection trip completed." << endl;
            return true;
      
    }
    return true;
}

void Inspector::start_inspecting(list<Gold_Mine*> mine){
    /*cout << display_code << get_id();
    cout << " starting to inspect." << endl;*/
    state = 'm';
    list<Gold_Mine*>::iterator j;
    for (j = mine.begin(); j != mine.end(); ++j){
        setup_destination((*j) -> get_location());
        
        if (this -> update()) {
            amount = (*j) -> get_amount();
            double * value = &amount;
            check_amount.push_back(value);
            cout << "This mine contains " << amount <<" of gold" <<endl;
            state = 'o';
        }
    }
    
}//inspect the mines.

void Inspector::finish_inspecting(list<Gold_Mine*> mine){
    cout << "Starting back" << endl;
    mine.reverse();
    check_amount.reverse();
    
    list<Gold_Mine*>::iterator j;
    for (j = mine.begin(); j != mine.end(); ++j){
        setup_destination((*j) -> get_location());
        if (this -> update()) {
            amount = amount - ((*j)->get_amount());
            cout << "Amount of gold removed from this mine is " << amount << endl;
        }
    }
    
     
     
    
}//call for the return journey.


Inspector::~Inspector(){ //call inspector destructor
    cout << "Inspector destructed." << endl;
}
