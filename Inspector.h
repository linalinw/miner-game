#ifndef Inspector_h
#define Inspector_h

#include "Game_Object.h"
#include "Person.h"
#include "Gold_Mine.h"
#include "Town_Hall.h"
#include "Cart_Vector.h"
#include "Cart_Point.h"
#include <list>

class Inspector : public Person{
private:
    char display_code;
    int id_num;
    double amount; //used to check the amount of gold reoved
    Cart_Point init_pos;
    list<Gold_Mine*> mine;

    
public:
    Inspector();//define the default constructor
    
    Inspector(int in_id, Cart_Point in_loc);
    
    bool update();
    
    void show_status();
    
    void start_inspecting(list<Gold_Mine*>);
    
     list<double*> check_amount;
    
    void finish_inspecting(list<Gold_Mine*> mine);
    
    ~Inspector();//destructor.

};

#endif
