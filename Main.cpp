
#include <iostream>

#include "Game_Object.h"
#include "Person.h"
#include "Gold_Mine.h"
#include "Town_Hall.h"
#include "Cart_Vector.h"
#include "Cart_Point.h"
#include "Model.h"
#include "Game_Command.h"
#include "View.h"
#include "Soldier.h"
#include "Input_Handling.h"
#include "Inspector.h"

#include <string>
#include <sstream>

using namespace std;

int main(){
    //print the introduction at the beginning of each game
    cout << "EC327: Programming Assignment 4 -- Fall 2016" << endl;
    
    Model model;
    listing(model); //list the status of every game objects
    bool quiting = false; //declare the quit function
    char command;
    double ID1, ID2, ID3;
    char type;
    string ligne;
    
    while(!quiting){
        
        //draw the grid
        View viewing;
        model.display(viewing);
        
        //ask user to enter a command
        cout << "Enter command: ";
        getline(cin,ligne);
        
        istringstream line1(ligne);
        
        line1 >> command;
        try{
            if (command != 'm' && command != 'w' && command != 's' && command != 'g' && command != 'r' && command != 'l' && command != 'q' && command != 'a' && command != 'n'&& command != 'i') {
                throw Invalid_Input(&command);
            }//check if the command is valid
            
        
            switch(command){
                //calls the move command
                case'm':
                    line1 >> ID1 >> ID2 >> ID3;
                    move(model,(int)ID1,ID2,ID3);
                    break;
                
                //calls the work command
                case'w':
                    line1 >> ID1 >> ID2 >> ID3;
                    work(model,(int)ID1,(int)ID2,(int)ID3);
                    break;
                
                //calls the stop command
                case's':
                    line1 >> ID1;
                    stop(model,(int)ID1);
                    break;
                
                //calls the go function
                case'g':
                    go(model);
                    listing(model);//added part
                    break;
                
                //calls the run function
                case'r':
                    run(model);
                    listing(model);//added part
                    break;
        
                //calls the listing function
                case'l':
                    listing(model);
                    break;
            
                //calls the quit command
                case'q':
                    quit(model);
                    quiting = true;
                    break;
                
                //PA4 addition
                //calls the attack function.
                case 'a':
                    line1 >> ID1 >> ID2;
                    attack(model,(int)ID1,(int)ID2);//write the attack function
                    break;
                    
                case 'i':
                    line1 >> ID1;
                    inspect(model,(int)ID1);//start inspecting
                    break;
                    
                case 'n':
                    line1 >> type >> ID1 >> ID2 >> ID3;
                    if (type != 's' && type != 't' && type != 'g' && type != 'm' &&type != 'i') {//check that the type of object is correct.
                        throw Invalid_Input(&type);
                    }
                    
                    if (ID1 <= 0){//check if the ID is a valid one
                        throw Invalid_Input("Was expecting a number greater than 0.");
                    }
                    
                    if(ID2 > 20 || ID3 > 20){ //check if the object is on the graph
                        throw Invalid_Input("Object is plotted outside the grid.");
                    }
                    
                    if (ID1 > 10){ //check if the id of the new object is within range
                        char num = (char)ID1;
                        throw Invalid_Input(&num);
                    }
                    
                    if (ID2 > 20 || ID3 > 20){ //check if the new object is on the grid
                        char num = (char)ID2;
                        
                        throw Invalid_Input(&num);
                    }
                    
                    else { //create a new object if everything is good.
                        create(model, type, ID1, (double)ID2, (double)ID3);
                        break;
                    }
                    
                    
            }
        }
        
        catch (Invalid_Input& except){
            cout << "Invalid input - " << except.msg_ptr << endl;
            // actions to be taken if the input is wrong
        }
    };
    
    return 0;
}
 
/*
 PA4 stuff to add.
 a ID1 ID2 - attack - command Person ID1 to start attacking Person ID2. Check for invalid id numbers and input errors as with the other command functions. Note that any Person can be commanded to attack any other Person, but only Soldiers will actually do it (hence why you need the virtual function start_attack in Person).
*/
