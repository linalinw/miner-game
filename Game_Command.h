#ifndef Game_Command_h
#define Game_Command_h

#include "Game_Object.h"
#include "Person.h"
#include "Gold_Mine.h"
#include "Town_Hall.h"
#include "Cart_Vector.h"
#include "Cart_Point.h"
#include "Model.h"
#include <string>
#include <list>


void listing(Model&);

void move(Model&,int,double,double);

void work(Model&,int,int,int);

void stop(Model&,int);

void go(Model&);

void run(Model&);

void quit(Model&);

//PA4 addition
void attack(Model&,int,int);

void create(Model&,char,int,double,double);

void inspect(Model&,int);

#endif

/*
 Here is a description of the commands and their input values:
 • g1 is a Gold_Mine, ID number is 1, initial location is (1, 20).
 • g2 is a Gold_Mine, ID number is 2, initial location is (10, 20).
 • m1 is a Miner, ID number is 1, initial location is (1, 1).
 • m2 is a Miner, ID number is 2, initial location is (10, 1).
 • t1 is a Town_Hall, ID number is 1, location is (5, 0).
 Here is a description of the commands and their input values:
 • m ID x y
    o "move": command Miner ID to move to location (x, y)
 • w ID1 ID2 ID3
    o "work a mine": command Miner ID1 to start mining at Gold_Mine ID2 and deposit
 the gold at Town_Hall ID3.
 • s ID
    o "stop": command Miner ID to stop doing whatever it is doing
 • g
    o "go": advance one time step by updating each object once.
 • r
    o "run": advance one time step and update each object, and repeat until either the
 update function returns true for at least one of the objects, or 5 time steps have
 been done.
 • q
 o "quit": terminate the program
*/
