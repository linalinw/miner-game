#include "Cart_Vector.h"
#include <iostream>
#include "Cart_Point.h"


using namespace std;



Cart_Vector::Cart_Vector()
{
	x = 0.0;
	y = 0.0;
}

Cart_Vector::Cart_Vector(double inputx, double inputy)
{

	x = inputx;
	y = inputy;
	
	
};
ostream& operator<<( ostream& os, const Cart_Vector& v1){
	os << "<" << v1.x << " ," << v1.y << "> ";
	return os;
}

Cart_Vector operator*(const Cart_Vector& v1, const double d){
	Cart_Vector cart_vector;
	cart_vector.x = (v1.x * d);
	cart_vector.y = (v1.y * d);
	return cart_vector;
}

Cart_Vector operator/(const Cart_Vector& v1, const double d){
	if (d == 0){
		return v1;
	}

	else{
		Cart_Vector cart_vector;
		cart_vector.x = (v1.x / d);
		cart_vector.y = (v1.y / d);
		return cart_vector;
	}
}
