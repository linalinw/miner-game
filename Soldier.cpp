#include <iostream>

using namespace std;

#include "Soldier.h"

//maybe needed
Soldier::Soldier(): Person('S'){
    display_code = 'S';
    attack_strength = 2;
    range = 2.0;
    health = 5;
    cout << "Soldier default constructed." << endl;
}

Soldier::Soldier(int in_id, Cart_Point in_loc): Person('S',in_id,in_loc){
    display_code = 'S';
    attack_strength = 3;
    range = 2.0;
    health = 5;
    cout << "Soldier constructed." << endl;
}

//needed
void Soldier::start_attack(Person *in_target){
    if (this->is_alive()) {
        double distance;
        distance = cart_distance(location,in_target -> get_location()); //check this distance calculation
        if (distance <= range) {
            cout << "S" << this->get_id()<< ": Attacking." << endl;
            target = in_target;
            state = 'a';
            
        }
        
        else {
            cout << "S" << get_id();
            cout << ": Target is out of range." << endl;
        }
        
    }
    
    else { //do stuff if soldier is not dead
        cout << "He is dead. :(" << endl;
        }
}


bool Soldier::update(){
    switch (state) {
        case 'x': //if the soldier is dead
            return false;
            break;
            
        case 's'://if the soldier is stopped
            return false;
            break;
            
        case 'm':
            if(update_location()){
                state = 's';//if the soldier reached its destination
                return true;
            }
            
            else{
                state = 'm';
                return false;
            }
            break;
        
        case 'a':
            double distance;
            distance = cart_distance(location,target -> get_location()); //check this distance calculation
            //if in range check health
            
            if (distance <= range) {
                if (target -> get_health() <= 0) {//if dead
                    cout << "I triumph!" << endl;
                    state = 's';
                    return true;
                    
                }
                
                else {
                    cout << display_code << this -> get_id()<<":Take that!" << endl;
                    if(target->get_display_code() == 'S' || target->get_display_code() == 's'){
                        dynamic_cast<Soldier*>(target) -> take_hit(attack_strength,this);
                        cout << "S" << target->get_id()<< ": Attacking." << endl;
                        cout << display_code << target -> get_id()<<":Take that!" << endl;
                        cout << display_code << this -> get_id()<<": Ouch!" << endl;
                    }
                    
                    else{
                        target -> take_hit(attack_strength,this);
                    }
                    
                    
                    //changed for extra credit
                    state = state;
                    return false;
                }
            }
            
            else {
                cout << "S" << get_id();
                cout << ":Target is out of range." << endl;
                cout << "S" << get_id();
                cout <<":Chaaaaarge" << endl;//adjusted after sample output comes out
                state = 's';
                return true;
            }
            
            break;
           
    }
    return false;
}

void Soldier::show_status(){
    cout << "Soldier status: ";
    Person::show_status();
    switch (state) {
        case 's': // does nothing
            cout << " is stopped." << endl;
            break;
            
        case 'm': //moving to a destination
            cout << endl;
            break;
            
        case 'a':
            cout << " is attacking" << endl;
            break;
            
        case 'x': // dead.
            cout << " is dead." << endl;
            break;
            
    }
}

void Soldier::take_hit(int attack_strength, Person *attacker_ptr){
    health = health - attack_strength; //needs to be double-checked.

    if (health < 3){//change the display code from uppercase to lowercase
        display_code =(char)((int)display_code+32);
    }
    
    if(health <= 0){
        cout << display_code << get_id();
        cout << ": Ahhhh, I am dying." << endl;
        state = 'x';
    }

    else{
        //no changes to state
        cout << display_code << get_id()<< ": Ouch!" << endl;
        this -> start_attack(attacker_ptr);
    }
}
Soldier::~Soldier(){
    cout << "Soldier destructed." << endl;
}
