#ifndef CARTVECTOR_H
#define CARTVECTOR_H

#include <iostream>

using namespace std;

class Cart_Vector{
public:
	double x;
	double y;

	Cart_Vector();

	Cart_Vector(double inputx, double inputy);
    
};


Cart_Vector operator*(const Cart_Vector& ,const double );

Cart_Vector operator/(const Cart_Vector& , const double );

ostream& operator<<(ostream& os, const Cart_Vector&);


#endif