#ifndef Model_h
#define Model_h

#include "View.h"
#include "Game_Object.h"
#include "Person.h"
#include "Gold_Mine.h"
#include "Town_Hall.h"
#include "Cart_Vector.h"
#include "Cart_Point.h"
#include "Miner.h"
#include "Soldier.h"
#include "Inspector.h"
#include <list>

class Model{
public:
    Model();
    /*
     - It initializes the time to 0, then creates the objects using new, and stores the pointers to them in the arrays as follows:
     - The listing shows the object type, its id number, initial location, and subscript in object_ptrs, and the subscript in the other array.
     - Miner 1 (5, 1), object _ptrs[0], person_ptrs[0]
     - Miner 2 (10, 1), object _ptrs[1], person_ptrs[1]
     - Gold_Mine 1 (1, 20), object_ptrs[2], mine_ptrs[0]
     - Gold_Mine 2 (10, 20), object_ptrs [3], mine_ptrs [1]
     - Town_Hall - default constructed - object_ptrs[4], hall_ptrs[0]
     - Set num_objects to 5, num_persons to 2, num_mines to 2, num_halls to 1;
     - Finally, output a message: "Model default constructed";
     */
    
    ~Model();
    /*
     the destructor deletes each object, and outputs a message: "Model destructed.”
     Foo::~Foo()
     */
    
    Person *get_Person_ptr(int id);
    
    Gold_Mine *get_Gold_Mine_ptr(int id);
    
    Town_Hall *get_Town_Hall_ptr(int id);
    
    bool update();
    /*
     It provides a service to the main program. It increments the time, and iterates
     through the object_ptrs array and calls update() for each object. Since
     Game_Object::update() will be made virtual, this will update each object.
     */
    
    
    void display(View &view);
    /*
     Likewise it provides a service to the main program. It outputs the time,and generates the view display for all of the Game_Objects
     */
    
    void show_status();
    /*
     It outputs the status of all of the Game_Objects by calling their show_status() function.
     */
    
    //PA4 addition
    void add_person(Person*);//add a new miner or soldier to the list
    
    void add_mine(Gold_Mine*);//add a new gold mine to the list
    
    void add_townhall(Town_Hall*);//add a new town hall in the list.
    
    list<Gold_Mine*> getmine();
    
    

private:
    // make the copy constructor private to prevent a Model object from being copied
    
    int time; //the simulation time

//We have a set of arrays of pointers and a variable for the number in each array:
    //Game_Object *object_ptrs[10]; PA4 changes
    //int num_objects;
    list<Game_Object*> object_ptrs;
    
    //Person *person_ptrs[10]; PA4 changes
    //int num_persons;
    list<Person*> person_ptrs;
    
    //Gold_Mine *mine_ptrs[10]; PA4 changes
    //int num_mines;
    list<Gold_Mine*> mine_ptrs;
    
    //Town_Hall *hall_ptrs[10];
    //int num_halls;
    list<Town_Hall*> hall_ptrs;
    
    
    //copy constructor made private
    Model( const Model&);
    
    //PA4 addition
    list<Game_Object*> active_ptrs;
    
    
    
};


#endif 

/*
 Modify the Model class to create two Soldier objects and put their pointers in the object_ptrs array and the person_ptrs array as follows:
 
 Soldier 3, (5, 15), object_ptrs[5], person _ptrs[2]
 Soldier 4, (10, 15), object_ptrs[6], person _ptrs[3]
 
*/
