#include "Game_Object.h"
#include "Cart_Point.h"

#ifndef Town_Hall_h
#define Town_Hall_h

class Town_Hall: public Game_Object{
private:
    int id_num;
    double amount;
    
public:
    Town_Hall();
    Town_Hall(int inputId, Cart_Point inputLoc);
    

    void deposit_gold(double deposit_amount);
    bool update();
    void show_status();
    virtual ~Town_Hall();
};

#endif /* Town_Hall_h */