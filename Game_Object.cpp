#include "Game_Object.h"
#include <iostream>

using namespace std;

Game_Object::Game_Object(char in_code, int in_id)
{
    display_code = in_code;       //initialize the display_code
    id_num = in_id;             //initialize the id_num
    location.x = 0.0;     //set the x-coord of location to 0
    location.y = 0.0;     //set the y-coord of location to 0
    state = 's';
    
    
    cout << "Game_Object default constructed." << endl;
}

Game_Object::Game_Object(char in_code, int in_id, Cart_Point in_loc)
{
    display_code = in_code;
    id_num = in_id;
    location.x = in_loc.x;
    location.y = in_loc.y;
    state = 's';
    
    cout << "Game_Object constructed." << endl;
}

int Game_Object::get_id()
{
    return id_num;
}

void Game_Object::show_status()
{
    cout << display_code << " with ID " << Game_Object::get_id() << " at location " << location << endl;
}

Cart_Point Game_Object::get_location()
{
    return location;
}

Game_Object::~Game_Object(){
    cout << "Game_Object destructed." << endl;
}

void Game_Object::drawself(char * ptr){
    ptr[0] = display_code;
    ptr[1] = (id_num + 48);
}

//PA4 addition
char Game_Object::get_display_code(){
    return display_code;
}

bool Game_Object::is_alive(){
    if(state != 'x') //state x is for dead.
        return true;
    
    else
        return false;
}
