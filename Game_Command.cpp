#include "Game_Command.h"
#include <iostream>

using namespace std;

//list command
void listing(Model& model){
    model.show_status();
}

//move command - gotta ask how to differentiate between soldier and miner
void move(Model& model, int id, double x, double y){

    Person *worker = model.get_Person_ptr(id);//get the pointer to the required miner
    Cart_Point dest;
    dest.x = x;
    dest.y = y;
    
    worker -> start_moving(dest); //make the miner move
    
}

//work command
void work(Model& model,int ID1,int ID2,int ID3){
    //cout << "w " << ID1 << " " << ID2 << " " << ID3 << endl;
    Person *worker = model.get_Person_ptr(ID1);
    Gold_Mine *mine = model.get_Gold_Mine_ptr(ID2);
    Town_Hall *townhall = model.get_Town_Hall_ptr(ID3);
    
    //PA4 addition check the display code of the worker
    if(worker -> get_display_code() == 'M' || worker -> get_display_code() == 'm'){
        cout << "Miner " << ID1 << " mining at Gold_Mine " << ID2<<" and depositing at Town_Hall " << ID3<<"." << endl;
        cout <<"M"<<ID1 <<": Yes, my lord." << endl;
        
        dynamic_cast<Miner*>(worker) -> start_mining(mine, townhall);
    }
    
    else{
        worker -> start_mining();
    }

}

//stop command - gotta ask how to differentiate between soldier and miner
void stop(Model& model,int ID1){
    cout << "Stopping " << ID1 << endl;
    
    Person *worker = model.get_Person_ptr(ID1);
    //edited part for stop function
    worker -> stop();
}

void go(Model& model){
    cout << "Advancing one tick." << endl;
    //cout << "g" << endl;
    model.update();
}

void run(Model& model){
    //cout << "r" << endl;
    cout <<"Advancing to next event." << endl;
    for(int i = 0; i < 5; i++)
        if(model .update()){
            break;
        }
    }

void quit(Model& model){
    cout << "Terminating program." << endl;
}

//PA4 starts from here.
//attack command
void attack(Model& model,int ID1, int ID2){
    Person* fighter = model.get_Person_ptr(ID1);
    Person* victim = model.get_Person_ptr(ID2);
    
    //PA4 addition. Check if the display codeof the fighter.
    if(fighter -> get_display_code() == 'S'){
        dynamic_cast<Soldier*>(fighter) ->start_attack(victim);
    }
    
    else{
        fighter -> start_attack();
    }
}

//create command
void create(Model& model, char type, int ID1, double ID2, double ID3){
    Cart_Point in_loc(ID2,ID3);//define the location of the new game object
    switch (type) {
        case 't':
            if(model.get_Town_Hall_ptr(ID1)){
                cout << "ID number already exists!" << endl;
            }//check if ID is already taken
            
            else{
                Town_Hall* t = new Town_Hall(ID1, in_loc); //create a Town Hall.
                model.add_townhall(t);
            }//end of else statement
            break;
            
        case 'g':
            if(model.get_Gold_Mine_ptr(ID1)){
                cout << "ID number already exists!" << endl;
            }//check if ID is already taken
            
            else{
                Gold_Mine* t = new Gold_Mine(ID1, in_loc); //create a Gold Mine.
                model.add_mine(t);
            }//end of else statement
            break;
        
        case 'm':
            if(model.get_Person_ptr(ID1)){
                cout << "ID number already exists!" << endl;
            }//check if ID is already taken
            
            else{
                Miner* t = new Miner(ID1, in_loc); //create a Miner.
                model.add_person(t);
            }//end of else statement
            break;
        
        case 's':
            if(model.get_Person_ptr(ID1)){
                cout << "ID number already exists!" << endl;
            }//check if ID is already taken
            
            else{
                Soldier* t = new Soldier(ID1, in_loc); //create a Soldier.
                model.add_person(t);
            }//end of else statement
            break;
        
        case 'i':
            if(model.get_Person_ptr(ID1)){
                cout << "ID number already exists!" << endl;
            }//check if ID is already taken
            
            else{
                Inspector* t = new Inspector(ID1, in_loc); //create a Soldier.
                model.add_person(t);
            }//end of else statement
            break;

 
    }
}

void inspect(Model& model, int id){
    Person *inspector = model.get_Person_ptr(id);
    if(inspector->get_display_code() == 'I'||inspector->get_display_code() == 'i'){
        dynamic_cast<Inspector*>(inspector) -> start_inspecting(model.getmine());
    }
}


