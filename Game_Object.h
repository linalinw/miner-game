#ifndef GAMEOBJECT_h
#define GAMEOBJECT_h

#include "Cart_Point.h"
#include "Cart_Vector.h"

class Game_Object{
private:
    int id_num;
    
protected:
    //child classes.
    Cart_Point location;
    char display_code;
    char state;
    
public:
    Game_Object (char in_code,int in_id);
    Game_Object(char,int,Cart_Point);
    Cart_Point get_location();
    int get_id();
    virtual void show_status();
    virtual bool update() = 0; //declare a pure virtual function for bool update()
    virtual ~Game_Object(); //define the destructor. 
    void drawself(char * ptr);
    
    //PA4 addition
    char get_display_code();
    virtual bool is_alive();
};


#endif /* Game_Object_h */
