#ifndef View_h
#define View_h

#include "Game_Object.h"
#include "Person.h"
#include "Gold_Mine.h"
#include "Town_Hall.h"
#include "Cart_Vector.h"
#include "Cart_Point.h"

#include <iomanip>

const int view_maxsize = 20;

class View{
private:
    int size;
    /*
     the current size of the grid to be displayed; not all of the grid array will be
     displayed in this project.
     */
    
    double scale;
    /*
      the distance represented by each cell of the grid
     */
    
    Cart_Point origin;
    /*
      the coordinates of the lower left-hand corner of the grid
     */
    
    char grid[view_maxsize][view_maxsize][2];
    /*
      an array to hold the characters that make up the display grid.
     */
    
    bool get_subscripts(int &ix, int &iy, Cart_Point location);
    /*
     This function calculates the column and row subscripts of the grid array that correspond to the supplied location. Note that the x and y values corresponding to the subscripts can be calculated by (location - origin) / scale. Assign these to integers to truncate the fractional part to get the integer subscripts, which are returned in the two reference parameters. The function returns true if the subscripts are valid, that is within the size of the grid being displayed. If not, the function prints a message: "An object is outside the display" and returns false
     */
    
public:
    View();
    /* 
     ~~done
     It sets the size to 11, the scale to 2, and lets the origin default to (0, 0). No constructor output message is needed.
     */
    
    void clear();
    /*
     It sets all the cells of the grid to the background pattern shown in the sample output.
     */
    
    void plot(Game_Object * ptr);
    /*
     It plots the pointed-to object in the proper cell of the grid. It calls get_subscripts and if the subscripts are valid, it calls drawself() for the object to insert the appropriate characters in the cell of the grid. However, if there is already an object plotted in that cell of the grid, the characters are replaced with ‘*’ and ' ' to indicate that there is more than one object in that cell of the grid.
     */
    
    void draw();
    /*
     outputs the grid array to produce the display like that shown in the sample output. The size, scale, and origin are printed first, then each row and column, for the current size of the display. Note that the grid is plotted like a normal graph: larger x values are to the right, and larger y values are at the top. The x and y axes are labeled with values for every alternate column and row. Use the output stream formatting facilities to save the format settings, set them for neat output of the axis labels on the grid, and then restore them to their original settings. Specifications: Allow two characters for each numeric value of the axis labels, with no decimal points. The axis labels will be out of alignment and rounded off if their values cannot be represented well in two characters. This distortion is acceptable in the name of simplicity for this project
     */
};

#endif 
